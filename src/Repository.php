<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware;

use Debiturio\DoctrineMiddleware\DQL\FilteredCountSqlWalker;
use Debiturio\DoctrineMiddleware\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Ramsey\Uuid\UuidInterface;

class Repository
{

    public function __construct(protected EntityManagerInterface $em, private QueryBuilder $queryBuilder)
    {
    }

    public function add(object ...$entities): void
    {
        foreach ($entities as $entity) {
            if ($entity) {
                $this->em->persist($entity);
            }
        }
        $this->em->flush();
    }

    public function delete(object ...$entities): void
    {
        foreach ($entities as $entity) {
            $this->em->remove($entity);
        }
        $this->em->flush();
    }

    public function detach(object $entity): void
    {
        $this->em->detach($entity);
    }

    public function getById(string $entityClassName, UuidInterface $id): ?object
    {
        return $this->em->find($entityClassName, $id);
    }

    public function getEntities(string  $entityClassName,
                                array   $filter = [],
                                array   $sort = [],
                                array   $searchProperties = [],
                                string  $search = '',
                                bool    $findSimilar = true,
                                int     $limit = 100,
                                int     $offset = 0): array
    {
        $searchProperties = $this->filterSearchProperties($entityClassName, $searchProperties);
        return $this->queryBuilder->getQuery($entityClassName, $filter, $sort, $searchProperties, $search, $findSimilar, true, 'i', $offset, $limit)->execute();
    }

    public function getTotalNumberOfEntities(string $entityClassName): int
    {
        return (int) $this->queryBuilder->getQuery(
            $entityClassName, [], [],[], '', false, false, 'count(i)')->getSingleScalarResult();
    }

    public function getNumberOfFilteredEntities(string  $entityClassName,
                                                array   $filter = [],
                                                array   $searchProperties = [],
                                                string  $search = '',
                                                bool    $findSimilar = true): int
    {
        $searchProperties = $this->filterSearchProperties($entityClassName, $searchProperties);

        $query = $this->queryBuilder->getQuery(
            $entityClassName,
            $filter,
            [],
            $searchProperties,
            $search,
            $findSimilar,
            false,
            'count(i), i',
            null,
            null,
            false
        );

        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, FilteredCountSqlWalker::class);

        return (int)$query->getSingleScalarResult();
    }

    public function filterSearchProperties(string $entityClassName, array $searchProperties): array
    {
        if (empty($searchProperties)) {
            $metaData = $this->em->getClassMetadata($entityClassName);
            $searchProperties = $metaData->fieldNames;
        }

        $searchProperties = [
            ...$searchProperties,
            ...$this->replaceAsterixInSearchProperties($entityClassName, $searchProperties)
        ];

        $searchProperties = array_unique($searchProperties);
        return $this->filterSearchPropertiesByDataType($entityClassName, $searchProperties);
    }

    // TODO add unit test
    public function filterSearchPropertiesByDataType(string $entityClassName, array $searchProperties): array
    {
        return array_filter($searchProperties, function (string $searchProperty) use ($entityClassName) {

            $filterKeyItems = explode('.', $searchProperty);
            $key = lcfirst($filterKeyItems[0]);

            $metaData = $this->em->getClassMetadata($entityClassName);

            if ($metaData->hasField($key)) {
                return in_array($metaData->getTypeOfField($key), ['string', 'integer', 'float']);
            }

            if ($metaData->hasAssociation($key) && count($filterKeyItems) > 1) {
                return !empty($this->filterSearchPropertiesByDataType(
                    $metaData->getAssociationTargetClass($key),
                    [implode('.', array_slice($filterKeyItems, 1))]
                ));
            }

            return false;

        });
    }

    public function replaceAsterixInSearchProperties(string $entityClassName, array $searchProperties, string $prefix = null): array
    {
        $metaData = $this->em->getClassMetadata($entityClassName);

        $fields = [];

        $allFields = !empty(array_filter(
            $searchProperties,
            function ($property) {
                return str_starts_with('*', $property);
            }
        ));

        if ($allFields) {
            $fields = array_map(
                function (string $field) use ($prefix) {
                    return $prefix ? $prefix . '.' . $field : $field;
                },
                $metaData->getFieldNames()
            );
        }

        foreach ($searchProperties as $searchProperty) {

            $keyItems = explode('.', $searchProperty);

            if (count($keyItems) > 1) {

                $key = $keyItems[0];

                if ($key === '*') {

                    foreach ($metaData->getAssociationNames() as $associationName) {

                        $fields = [
                            ...$fields,
                            ...$this->replaceAsterixInSearchProperties(
                                $metaData->getAssociationTargetClass($associationName),
                                [implode('.', array_slice($keyItems, 1))],
                                $prefix ? $prefix . '.' . $associationName : $associationName
                            )
                        ];

                    }

                }

                if ($key !== '*' && $metaData->hasAssociation($key)) {

                    $fields = [
                        ...$fields,
                        ...$this->replaceAsterixInSearchProperties(
                            $metaData->getAssociationTargetClass($key),
                            [implode('.', array_slice($keyItems, 1))],
                            $prefix ? $prefix . '.' . $key : $key
                        )
                    ];

                }

            }

        }

        return $fields;
    }
}