<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\AST\PathExpression;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Similarity extends FunctionNode
{
    private Node|null $search = null;

    /** @var PathExpression[] */
    private array $fields = [];

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->search = $parser->StringPrimary();

        while ($parser->getLexer()->isNextToken(Lexer::T_COMMA)) {
            $parser->getLexer()->moveNext();
            $this->fields[] = $parser->StringPrimary();
        }


        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            "SIMILARITY(%s, cast ((%s) as varchar))",
            $this->search->dispatch($sqlWalker),
            implode("|| ' ' || ", array_map(function (PathExpression $pathExpression) use ($sqlWalker) {
                return $pathExpression->dispatch($sqlWalker);
            }, $this->fields))
        );
    }
}