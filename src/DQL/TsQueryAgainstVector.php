<?php

namespace Debiturio\DoctrineMiddleware\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\AST\PathExpression;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class TsQueryAgainstVector extends FunctionNode
{
    /** @var PathExpression[] */
    private array $fields = [];

    private Node|null $search = null;

    public function parse(Parser $parser) : void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->search = $parser->StringExpression();

        while ($parser->getLexer()->isNextToken(Lexer::T_COMMA)) {
            $parser->getLexer()->moveNext();
            $this->fields[] = $parser->StringPrimary();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /** @psalm-suppress all */
    public function getSql(SqlWalker $sqlWalker) : string
    {
        return sprintf(
            "to_tsquery(quote_literal(%s)) @@ to_tsvector(quote_literal(%s))",
            $this->search->dispatch($sqlWalker),
            implode(" || ' ' || ", array_map(function (PathExpression $pathExpression) use ($sqlWalker) {
                $fi = $pathExpression->dispatch($sqlWalker);
                return sprintf("coalesce(%s::text, '')", $fi);
            }, $this->fields)),
        );
    }
}