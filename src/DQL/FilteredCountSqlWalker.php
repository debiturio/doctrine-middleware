<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\DQL;

use Doctrine\ORM\Query\SqlWalker;

class FilteredCountSqlWalker extends SqlWalker
{
    public function walkSelectClause($selectClause)
    {
        return strtok(parent::walkSelectClause($selectClause), ',');
    }
}