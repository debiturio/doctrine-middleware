<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Handler;

use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CreateHandler extends AbstractHandler
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = is_array($request->getParsedBody()) ? $request->getParsedBody() : [$request->getParsedBody()];
        $this->repository->add(...$data);

        $ids = array_filter(array_map(function ($entity) {
            return method_exists($entity, 'getId') ? (string)$entity->getId() : null;
        }, $data));

        return new EmptyResponse(201, ['Location' => count($ids) === 1 ? $ids[0] : implode(',', $ids)]);
    }
}