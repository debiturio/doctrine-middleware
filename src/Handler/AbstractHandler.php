<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Handler;

use Debiturio\DoctrineMiddleware\Repository;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

abstract class AbstractHandler implements RequestHandlerInterface
{
    public function __construct(protected Repository $repository)
    {
    }
}