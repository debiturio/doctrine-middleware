<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Handler;

use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class UpdateHandler extends AbstractHandler
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = is_array($request->getParsedBody()) ? $request->getParsedBody() : [$request->getParsedBody()];
        $this->repository->add(...$data);

        return new EmptyResponse();
    }
}