<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ReadCollectionMiddleware extends AbstractReadMiddleware
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $entityClassName = $this->getClassName($request);

        $params = $request->getQueryParams();
        $sort = array_key_exists('sort', $params) ? explode(',', $params['sort']) : [];
        $filter = array_key_exists('filter', $params) ? $params['filter'] : [];
        $searchProperties = array_key_exists('searchProperties', $params) ? explode(',', $params['searchProperties']) : [];
        $properties = array_key_exists('properties', $params) ? explode(',', $params['properties']) : [];
        $findSimilar = !array_key_exists('findSimilar', $params) || strtolower($params['findSimilar']) === 'true';
        $search = array_key_exists('search', $params) ? $params['search'] : '';
        $limit = array_key_exists('limit', $params) ? (int) $params['limit'] : 100;
        $offset = array_key_exists('page', $params) ? (((int) $params['page']) - 1) * $limit : 0;
        $meta = array_key_exists('meta', $params) ? explode(',', $params['meta']) : [];

        $metaResult = [];

        if (empty($searchProperties)) {
            $searchProperties = $properties;
        }

        if (in_array('*', $meta) || in_array('filterCount', $meta)) {
            $metaResult['filterCount'] = $this->repository->getNumberOfFilteredEntities($entityClassName, $filter, $searchProperties, $search, $findSimilar);
        }

        if (in_array('*', $meta) || in_array('totalCount', $meta)) {
            $metaResult['totalCount'] = $this->repository->getTotalNumberOfEntities($entityClassName);
        }

        $result = ['data' => $this->repository->getEntities($entityClassName, $filter, $sort, $searchProperties, $search, $findSimilar, $limit, $offset)];

        if (!empty($metaResult)) $result['meta'] = $metaResult;

        return $handler->handle($request->withParsedBody($result));
    }
}