<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Middleware;

use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

class ReadSingleMiddleware extends AbstractReadMiddleware
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $entityClassName = $this->getClassName($request);

        $entity = $this->repository->getById($entityClassName, Uuid::fromString($request->getAttribute('id')));

        if (!$entity) return new EmptyResponse(404);

        return $handler->handle($request->withParsedBody($entity));
    }
}