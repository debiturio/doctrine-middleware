<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Middleware;

use Debiturio\DoctrineMiddleware\Repository;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;

abstract class AbstractReadMiddleware implements MiddlewareInterface
{
    /**
     * @param Repository $repository
     * @param array $endpointsToClasses
     * eg. [ '/foo' => [ 'post' => 'ClassName' ] ] or [ 'bar' => 'ClassName' ]
     */
    public function __construct(protected Repository $repository,
                                private array $endpointsToClasses)
    {
    }

    public function getClassName(ServerRequestInterface $request): string
    {
        /** @var RouteResult $routeResult */
        $routeResult = $request->getAttribute(RouteResult::class);

        $endpoint = $routeResult && array_key_exists($routeResult->getMatchedRouteName(), $this->endpointsToClasses) ?
            $this->endpointsToClasses[$routeResult->getMatchedRouteName()] : null;

        $className = is_array($endpoint) && array_key_exists(strtolower($request->getMethod()), $endpoint) ?
            $endpoint[$request->getMethod()] : null;

        if (!$className && is_string($endpoint)) return $endpoint;

        if (!$className) {
            throw new \Exception(
                sprintf(
                    'Missing object class definition for %s %s',
                    $request->getMethod(),
                    $request->getUri()->getPath()
                )
            );
        }

        return $className;
    }
}