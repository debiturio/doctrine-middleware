<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Query;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Composite;
use Doctrine\ORM\Query\Expr\Join;
use Ramsey\Uuid\Uuid;

class QueryBuilder
{
    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(private EntityManagerInterface $em)
    {
    }

    /**
     * @param string $entityClassName
     * @param array $filter
     * @param array $sort
     * @param string[] $searchProperties
     * @param string $search
     * @param bool $findSimilar
     * @param string $select
     * @param int|null $offset
     * @param int|null $limit
     * @param bool $selectLeftJoins
     * @return Query
     */
    public function getQuery(
        string  $entityClassName,
        array   $filter = [],
        array   $sort = [],
        array   $searchProperties = [],
        string  $search = '',
        bool    $findSimilar = true,
        bool    $orderBySimilarity = true,
        string  $select = 'i',
        ?int    $offset = 0,
        ?int    $limit = 100,
        bool    $selectLeftJoins = true): Query
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select($select)->from($entityClassName, 'i');

        $selectors = [
            'i' => [
                'parent' => '',
                'alias' => 'i'
            ]
        ];

        $expression = $this->getExpression($qb, $entityClassName, $selectors, $filter, $sort, $selectLeftJoins);

        if ($expression->count() > 0) {
            $qb->where($expression);
        }

        if (!empty($search)) {
            $this->addFullTextSearchExpression($qb, $selectors, $searchProperties, trim($search), $findSimilar, $orderBySimilarity);
        }

        if ($limit && $limit !== -1) $qb->setMaxResults($limit);
        if ($offset) $qb->setFirstResult($offset);

        return $qb->getQuery();
    }

    public function addFullTextSearchExpression(
        \Doctrine\ORM\QueryBuilder $qb,
        &$selectors,
        array   $searchProperties = [],
        string  $search = '',
        bool    $findSimilar = true,
        bool    $orderBySimilarity = true
    ): void
    {

        $fields = [];

        foreach ($searchProperties as $searchProperty) {

            $filterKeyItems = explode('.', $searchProperty);
            $parentKeyItems = array_slice($filterKeyItems, 0, -1);

            $this->addLeftJoin($parentKeyItems, $qb, $selectors, true);

            $parentPath = implode('.', $parentKeyItems);
            $alias = empty($parentPath) ? 'i' : $selectors[$parentPath]['alias'];
            $key = $this->camelize($filterKeyItems[count($filterKeyItems) - 1]);
            $fields[] = $alias . '.' . $key;
        }

        $search = implode(
            ' & ',
            array_filter(array_map(function (string $item) {
                return trim($item);
            }, explode(' ', $search)))
        );

        if ($orderBySimilarity) {
            $qb->addSelect('SIMILARITY(:searchKey, ' . implode(',', $fields) . ') AS HIDDEN similarity');
        }

        $tsVectorExpression = $qb->expr()->eq('TS_VECTOR(:searchKey, ' . implode(',', $fields) . ')', 'true');

        if ($findSimilar) {

            $exp = $qb->expr()->orX();
            $exp->add($tsVectorExpression);
            $exp->add('SIMILARITY(:searchKey, ' . implode(',', $fields) . ') > 0');

            $qb->andWhere($exp);

        } else {
            $qb->andWhere($tsVectorExpression);
        }

        $qb->setParameter('searchKey', $search);

        if ($orderBySimilarity) {
            $qb->orderBy('similarity', 'DESC');
        }
    }

    private function getExpression(\Doctrine\ORM\QueryBuilder $qb,
                                   string $entityClassName,
                                   array &$selectors,
                                   array $filter,
                                   array $sort = [],
                                   bool $selectLeftJoins = true,
                                   LogicalOperator $logicalOperator = LogicalOperator::AND): Composite
    {
        $expression = $logicalOperator === LogicalOperator::AND ? $qb->expr()->andX() : $qb->expr()->orX();

        foreach ($filter as $filterKey => $filterValue) {

            $filterKeyIsLogical = $filterKey === LogicalOperator::AND->value || $filterKey === LogicalOperator::OR->value;

            if ($filterKeyIsLogical) {
                $nestedExpression = $this->getExpression($qb,
                    $entityClassName,
                    $selectors,
                    $filterValue,
                    [],
                    $selectLeftJoins,
                    LogicalOperator::from($filterKey));

                $expression->add($nestedExpression);
            }

            if (!$filterKeyIsLogical) {

                if (!$this->fieldExists($entityClassName, $filterKey)) continue;

                $filterKeyItems = explode('.', $filterKey);
                $parentKeyItems = array_slice($filterKeyItems, 0, -1);

                $this->addLeftJoin($parentKeyItems, $qb, $selectors, $selectLeftJoins);

                $key = $this->camelize($filterKeyItems[count($filterKeyItems) - 1]);
                $parentPath = implode('.', $parentKeyItems);

                $alias = empty($parentPath) ? 'i' : $selectors[$parentPath]['alias'];

                $filterValue = is_string($filterValue) ? ['_eq' => $filterValue] : $filterValue;

                foreach ($filterValue as $operator => $value) {

                    $operatorKeyIsLogical = $operator === LogicalOperator::AND->value || $operator === LogicalOperator::OR->value;

                    if ($operatorKeyIsLogical) {

                        $nestedExpression = $this->getExpression($qb,
                            $entityClassName,
                            $selectors,
                            [$filterKey => $value],
                            [],
                            $selectLeftJoins,
                            LogicalOperator::from($operator),
                        );

                        $expression->add($nestedExpression);

                        continue;
                    }

                    $sqlOperator = $this->getOperator($operator, $value);

                    $uniqueKey = $alias.ucfirst($key);

                    if ($sqlOperator === 'LIKE' || $sqlOperator === 'NOT LIKE') {

                        $metaData = $this->em->getClassMetadata($entityClassName);
                        $type = $metaData->getTypeOfField($filterKey);

                        if ($type === 'string' || $type === 'text') {
                            $exp = sprintf('LOWER(%s.%s) %s LOWER(:%s)', $alias, $key, $sqlOperator, $uniqueKey);

                        } else {
                            $exp = sprintf('%s.%s %s :%s', $alias, $key, $sqlOperator, $uniqueKey);
                        }

                    } elseif ($sqlOperator === 'IS NULL' || $sqlOperator === 'IS NOT NULL') {
                        $exp = sprintf('%s.%s %s', $alias, $key, $sqlOperator);
                    } elseif ($sqlOperator === 'IN' || $sqlOperator === 'NOT IN') {
                        $exp = sprintf('%s.%s %s (:%s)', $alias, $key, $sqlOperator, $uniqueKey);
                    } else {
                        $exp = sprintf('%s.%s %s :%s', $alias, $key, $sqlOperator, $uniqueKey);
                    }

                    if (is_string($value) && ($sqlOperator === 'IN' || $sqlOperator === 'NOT IN')) {
                        $value = explode(',', $value);
                    }

                    $expression->add($exp);

                    if (!($sqlOperator === 'IS NULL' || $sqlOperator === 'IS NOT NULL')) {
                        $qb->setParameter($uniqueKey, $sqlOperator === 'LIKE' || $sqlOperator === 'NOT LIKE' ? '%' . $value . '%' : $value);
                    }

                }

            }

        }

        foreach ($sort as $sortItem) {

            $descending = str_starts_with($sortItem, '-');
            $sortItem = $descending ? substr($sortItem, 1) : $sortItem;

            if (!$this->fieldExists($entityClassName, $sortItem)) continue;

            $sortKeyItems = explode('.', $sortItem);
            $parentKeyItems = array_slice($sortKeyItems, 0, -1);

            $key = $this->camelize($sortKeyItems[count($sortKeyItems) - 1]);
            $parentPath = implode('.', $parentKeyItems);

            if (!empty($parentPath) && !array_key_exists($parentPath, $selectors)) {
                $this->addLeftJoin($parentKeyItems, $qb, $selectors, $selectLeftJoins);
            }

            $alias = empty($parentPath) ? 'i' : $selectors[$parentPath]['alias'];

            $qb->addOrderBy(sprintf('%s.%s', $alias, $key), $descending ? 'DESC' : 'ASC');

        }

        return $expression;
    }

    public function addLeftJoin(array $parentKeyItems, \Doctrine\ORM\QueryBuilder $qb, array &$selectors, bool $selectLeftJoins): void
    {
        $parentKeyPaths = array_map(
            function (string $value, int $index) use ($parentKeyItems) {
                return [
                    'item' => $value,
                    'path' => implode('.', array_slice($parentKeyItems, 0, $index + 1)),
                    'parentPath' => $index > 0 ? implode('.', array_slice($parentKeyItems, 0, $index)) : 'i',
                ];
            },
            $parentKeyItems,
            array_keys($parentKeyItems)
        );

        foreach ($parentKeyPaths as $item) {

            if (!array_key_exists($item['path'], $selectors)) {

                $alias = sprintf('i%s',count($selectors) + 1) ;

                $selectors[$item['path']] = [
                    'parent' => $item['parentPath'],
                    'alias' => $alias
                ];

                $qb->leftJoin(sprintf(
                    '%s.%s',
                    $selectors[$item['parentPath']]['alias'], $item['item']),
                    $alias
                );

                if ($selectLeftJoins) {
                    $qb->addSelect($alias);
                }

            }
        }
    }

    /**
     * @param string $entityClassName
     * @param string $path
     * @return bool
     */
    public function fieldExists(string $entityClassName, string $path): bool
    {
        $metadata = $this->em->getClassMetadata($entityClassName);

        $fields = explode('.', $path);

        if (count($fields) === 1) {
            return $metadata->hasField($this->camelize($path)) ||$metadata->hasAssociation($this->camelize($path));
        }

        $field = $this->camelize($fields[0]);

        if (count($fields) > 1 && $metadata->hasAssociation($field)) {
            return $this->fieldExists(
                $metadata->getAssociationTargetClass($field),
                implode('.', array_slice($fields, 1))
            );
        }

        return false;
    }

    /**
     * @param string $input
     * @param string $separator
     * @return string
     */
    private function camelize(string $input, string $separator = '_'): string
    {
        return str_replace($separator, '', lcfirst(ucwords($input, $separator)));
    }

    /**
     * @param string $operator
     * @return string
     */
    private function getOperator(string $operator = '=', mixed $value = null): string
    {
        if ($operator === '_null' && $value === 'false') {
            $operator = '_nnull';
        }

        if ($operator === '_nnull' && $value === 'true') {
            $operator = '_null';
        }

        return match ($operator) {
            '=' => '=',
            '_eq' => '=',
            '_neq' => '<>',
            '_lt' => '<',
            '_lte' => '<=',
            '_gt' => '>',
            '_gte' => '>=',
            '_in' => 'IN',
            '_nin' => 'NOT IN',
            '_null' => 'IS NULL',
            '_nnull' => 'IS NOT NULL',
            '_contains' => 'LIKE',
            '_ncontains' => 'NOT LIKE',
        };
    }
}