<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddleware\Query;

enum LogicalOperator: string
{
    case AND = '_and';
    case OR = '_or';
}