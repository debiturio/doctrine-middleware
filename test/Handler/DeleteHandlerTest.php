<?php

namespace Debiturio\DoctrineMiddlewareTest\Handler;

use Debiturio\DoctrineMiddleware\Handler\DeleteHandler;
use Debiturio\DoctrineMiddleware\Repository;
use Laminas\Diactoros\Response\EmptyResponse;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DeleteHandlerTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param array|object $data
     * @return void
     */
    public function testHandle(array|object $data)
    {
        $data = is_array($data) ? $data : [$data];
        $repository = $this->createMock(Repository::class);
        $repository->expects($this->once())->method('delete')->with(...$data);

        $request = $this->createStub(ServerRequestInterface::class);
        $request->method('getParsedBody')->willReturn($data);

        $middleware = new DeleteHandler($repository);
        $result = $middleware->handle($request, $this->createStub(RequestHandlerInterface::class));

        $this->assertInstanceOf(EmptyResponse::class, $result);
        $this->assertEquals(204, $result->getStatusCode());
    }

    public function dataProvider()
    {
        return [
            [
                new \stdClass()
            ],
            [
                [
                    new \stdClass(),
                    new \stdClass()
                ]
            ]
        ];
    }
}
