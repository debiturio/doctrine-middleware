<?php

namespace Debiturio\DoctrineMiddlewareTest\Handler;

use Debiturio\DoctrineMiddleware\Handler\UpdateHandler;
use Debiturio\DoctrineMiddleware\Repository;
use Laminas\Diactoros\Response\EmptyResponse;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class UpdateHandlerTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param array|object $data
     * @return void
     */
    public function testHandle(array|object $data)
    {
        $data = is_array($data) ? $data : [$data];
        $repository = $this->createMock(Repository::class);
        $repository->expects($this->once())->method('add')->with(...$data);

        $request = $this->createStub(ServerRequestInterface::class);
        $request->method('getParsedBody')->willReturn($data);

        $middleware = new UpdateHandler($repository);
        $result = $middleware->handle($request);

        $this->assertInstanceOf(EmptyResponse::class, $result);
        $this->assertEquals(204, $result->getStatusCode());
    }

    public function dataProvider()
    {
        return [
            [
                new \stdClass()
            ],
            [
                [
                    new \stdClass(),
                    new \stdClass()
                ]
            ]
        ];
    }
}
