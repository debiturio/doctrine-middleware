<?php

namespace Debiturio\DoctrineMiddlewareTest\Handler;

use Debiturio\DoctrineMiddleware\Handler\CreateHandler;
use Debiturio\DoctrineMiddleware\Repository;
use Debiturio\DoctrineMiddlewareTest\TestClass\TestEntity;
use Laminas\Diactoros\Response\EmptyResponse;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

class CreateHandlerTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param array|object $data
     * @param string $location
     * @return void
     */
    public function testHandle(array|object $data, string $location)
    {
        $data = is_array($data) ? $data : [$data];
        $repository = $this->createMock(Repository::class);
        $repository->expects($this->once())->method('add')->with(...$data);

        $request = $this->createStub(ServerRequestInterface::class);
        $request->method('getParsedBody')->willReturn($data);

        $middleware = new CreateHandler($repository);
        $result = $middleware->handle($request);

        $this->assertInstanceOf(EmptyResponse::class, $result);
        $this->assertEquals(201, $result->getStatusCode());
        $this->assertEquals($location, $result->getHeader('Location')[0]);
    }

    public function dataProvider()
    {
        return [
            [
                new TestEntity($id = Uuid::uuid4()),
                $id->toString()
            ],
            [
                [
                    new TestEntity($idOne = Uuid::uuid4()),
                    new TestEntity($idTwo = Uuid::uuid4())
                ],
                $idOne->toString() . ',' . $idTwo
            ]
        ];
    }
}
