<?php
declare(strict_types=1);

namespace Debiturio\DoctrineMiddlewareTest\TestClass;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class TestEntity
{
    public function __construct(private UuidInterface $id)
    {

    }

    /**
     * @return Uuid
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }
}