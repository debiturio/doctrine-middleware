<?php

namespace Debiturio\DoctrineMiddlewareTest\Middleware;

use Debiturio\DoctrineMiddleware\Middleware\ReadSingleMiddleware;
use Debiturio\DoctrineMiddleware\Repository;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

class ReadSingleMiddlewareTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param array $config
     * @param string $className
     * @param string $path
     * @param string $method
     * @param string $id
     * @return void
     */
    public function testProcess(array $config, string $className, string $path, string $method, string $id)
    {
        $repository = $this->createMock(Repository::class);

        $uri = $this->createStub(UriInterface::class);
        $uri->method('getPath')->willReturn($path);

        $entity = new \stdClass();
        $repository->method('getById')->with($className, Uuid::fromString($id))->willReturn($entity);

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn($method);
        $request->method('getUri')->willReturn($uri);
        $request->method('getAttribute')->with('id')->willReturn($id);

        $request->expects($this->once())->method('withParsedBody')
            ->with($entity)
            ->willReturn($requestResult = $this->createStub(ServerRequestInterface::class));

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->expects($this->once())->method('handle')->with($requestResult)
            ->willReturn($response = $this->createStub(ResponseInterface::class));

        $middleware = new ReadSingleMiddleware($repository, $config);

        $this->assertEquals($response, $middleware->process($request, $handler));
    }

    public function dataProvider()
    {
        $config = [
            '/ping' => [
                'post' => \stdClass::class,
                'put' => \stdClass::class
            ],
            '/test' => \stdClass::class
        ];

        return [
            [
                $config,
                \stdClass::class,
                '/test',
                'GET',
                Uuid::uuid4()->toString()
            ]
        ];
    }
}
