<?php

namespace Debiturio\DoctrineMiddlewareTest\Middleware;

use Debiturio\DoctrineMiddleware\Middleware\AbstractReadMiddleware;
use Debiturio\DoctrineMiddleware\Repository;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class AbstractReadMiddlewareTest extends TestCase
{

    /**
     * @dataProvider getClassNameProvider
     * @param array $config
     * @param string $path
     * @param string $method
     * @param string $expectedClassName
     * @return void
     * @throws \Exception
     */
    public function testGetClassName(array $config, string $path, string $method, string $expectedClassName)
    {
        $middleware = $this->getMockForAbstractClass(
            AbstractReadMiddleware::class,
            [
                $this->createStub(Repository::class),
                $config
            ]
        );

        $request = $this->createStub(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn($method);

        $uri = $this->createStub(UriInterface::class);
        $uri->method('getPath')->willReturn($path);

        $request->method('getUri')->willReturn($uri);

        $this->assertEquals($expectedClassName, $middleware->getClassName($request));
    }

    public function getClassNameProvider()
    {
        $config = [
            '/ping' => [
                'post' => \stdClass::class,
                'put' => \stdClass::class
            ],
            '/test' => \stdClass::class
        ];

        return [
            [
                $config,
                '/ping',
                'post',
                \stdClass::class
            ],
            [
                $config,
                '/ping',
                'put',
                \stdClass::class
            ],
            [
                $config,
                '/test',
                'put',
                \stdClass::class
            ]
        ];
    }
}
