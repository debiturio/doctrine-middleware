<?php

namespace Debiturio\DoctrineMiddlewareTest\Middleware;

use Debiturio\DoctrineMiddleware\Middleware\ReadCollectionMiddleware;
use Debiturio\DoctrineMiddleware\Query\QueryBuilder;
use Debiturio\DoctrineMiddleware\Repository;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ReadCollectionMiddlewareTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param array $config
     * @param string $className
     * @param string $path
     * @param string $method
     * @param array $params
     * @return void
     */
    public function testProcess(array $config,
                                string $className,
                                string $path,
                                string $method,
                                array $params)
    {
        $t = ($params['page'] - 1) * (array_key_exists('limit', $params) ? $params['limit'] : 100);
        $repository = $this->createMock(Repository::class);
        $repository->method('getEntities')
            ->with(
                $className,
                $params['filter'],
                array_key_exists('limit', $params) ? $params['limit'] : 100,
                array_key_exists('page', $params) ?
                    ($params['page'] - 1) * (array_key_exists('limit', $params) ? $params['limit'] : 100) : 0
            )
            ->willReturn($repositoryResult = [new \stdClass(), new \stdClass()]);

        $uri = $this->createStub(UriInterface::class);
        $uri->method('getPath')->willReturn($path);

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn($method);
        $request->method('getUri')->willReturn($uri);

        $meta = [];

        if (array_key_exists('meta', $params)) {
            if ($params['meta'] === '*' || in_array('filter_count', $params['meta'])) {
                $repository->method('getNumberOfFilteredEntities')
                    ->with($className, $params['filter'])
                    ->willReturn(150);

                $meta['filter_count'] = 150;
            }

            if ($params['meta'] === '*' || in_array('total_count', $params['meta'])) {
                $repository->method('getTotalNumberOfEntities')->with($className)->willReturn(1000);
                $meta['total_count'] = 1000;
            }
        }

        $request->expects($this->once())->method('withParsedBody')
            ->with(['data' => $repositoryResult, 'meta' => $meta])
            ->willReturn($requestResult = $this->createStub(ServerRequestInterface::class));

        $request->method('getQueryParams')->willReturn($params);

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->expects($this->once())->method('handle')->with($requestResult)
            ->willReturn($response = $this->createStub(ResponseInterface::class));

        $middleware = new ReadCollectionMiddleware($repository, $config);

        $this->assertEquals($response, $middleware->process($request, $handler));
    }

    public function dataProvider()
    {
        $config = [
            '/ping' => [
                'post' => \stdClass::class,
                'put' => \stdClass::class
            ],
            '/test' => \stdClass::class
        ];

        return [
            [
                $config,
                \stdClass::class,
                '/test',
                'GET',
                [
                    'filter' => 'foo[]',
                    'limit' => 30,
                    'page' => 3,
                    'meta' => '*'
                ],
                 ['foo' => 'bar']
            ]
        ];
    }
}
