<?php

namespace Debiturio\DoctrineMiddlewareTest\Query;

use Debiturio\DoctrineMiddleware\Query\QueryBuilder;
use DG\BypassFinals;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\Persistence\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{
    protected function setUp(): void
    {
        BypassFinals::enable();
        parent::setUp();
    }

    /**
     * @dataProvider fieldExistsDataProvider
     * @param array $existingFields
     * @param array $existingAssociations
     * @param string $path
     * @param string $entityClassName
     * @param bool $expectedResult
     * @return void
     */
    public function testFieldExists(array $existingFields,
                                    array $existingAssociations,
                                    string $path,
                                    string $entityClassName,
                                    bool $expectedResult)
    {
        $metadata = $this->createMock(ClassMetadata::class);
        $metadata->method('hasField')
            ->withConsecutive(...array_map(
                    function ($item) {
                        return [$item];
                    }, array_keys($existingFields))
            )
            ->willReturnOnConsecutiveCalls(...$existingFields);

        $metadata->method('hasAssociation')
            ->withConsecutive(...array_map(
                    function ($item) {
                        return [$item];
                    }, array_keys($existingAssociations))
            )
            ->willReturnOnConsecutiveCalls(...$existingAssociations);

        $metadata->method('getAssociationTargetClass')
            ->withConsecutive(...array_map(
                    function ($item) {
                        return [$item];
                    }, array_keys($existingAssociations))
            )
            ->willReturn($entityClassName);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getClassMetadata')->with($entityClassName)->willReturn($metadata);

        $builder = new QueryBuilder($em);

        $this->assertEquals($expectedResult, $builder->fieldExists($entityClassName, $path));
    }

    public function fieldExistsDataProvider()
    {
        return [
            [
                [
                    'firstName' => true
                ],
                [],
                'first_name',
                'ClassName',
                true
            ],
            [
                [
                    'name' => true
                ],
                [],
                'name',
                'ClassName',
                true
            ],
            [
                [
                    'name' => false
                ],
                [],
                'name',
                'ClassName',
                false
            ],
            [
                [
                    'street' => true
                ],
                [
                    'address' => true
                ],
                'address.street',
                'ClassName',
                true
            ],
            [
                [
                    'street' => false
                ],
                [
                    'address' => true
                ],
                'address.street',
                'ClassName',
                false
            ],
            [
                [],
                [],
                'address.street',
                'ClassName',
                false
            ],
            [
                [
                    'street' => true
                ],
                [
                    'address' => true,
                    'user' => true
                ],
                'address.user.street',
                'ClassName',
                true
            ],
        ];
    }

    /**
     * @dataProvider getQueryDataProvider
     * @param string $entityClassName
     * @param array $filter
     * @param string $select
     * @param array $leftJoin
     * @param array $andWhere
     * @param array $orWhere
     * @param array $parameters
     * @param int|null $offset
     * @param int|null $limit
     * @return void
     */
    public function testGetQuery(string $entityClassName,
                                 array  $filter,
                                 string $select,
                                 array  $leftJoin,
                                 array  $andWhere,
                                 array  $orWhere,
                                 array  $parameters,
                                 int    $offset = null,
                                 int    $limit = null)
    {
        $qb = $this->createMock(\Doctrine\ORM\QueryBuilder::class);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->once())->method('createQueryBuilder')->willReturn($qb);

        $metadata = $this->createStub(ClassMetadata::class);
        $metadata->method('hasField')->willReturn(true);
        $metadata->method('hasAssociation')->willReturn(true);
        $metadata->method('getAssociationTargetClass')->willReturn('ClassName');
        $em->method('getClassMetadata')->willReturn($metadata);

        $qb->method('select')->with($select)->willReturn($qb);
        $qb->method('from')->with($entityClassName, 'i')->willReturn($qb);

        $qb->method('leftJoin')
            ->withConsecutive(...array_map(
                function ($item, $index) {
                    return [$item, sprintf('i%s', $index + 1)];
                },
                $leftJoin,
                array_keys($leftJoin)
                )
            );

        $qb->method('addSelect')
            ->withConsecutive(...array_map(
                    function ($index) {
                        return [sprintf('i%s', $index + 1)];
                    },
                    array_keys($leftJoin)
                )
            );

        $qb->expects($this->exactly(count($andWhere)))
            ->method('andWhere')
            ->withConsecutive(...array_map(
                    function ($item) {
                        return [$item];
                    },
                    $andWhere
                )
            );

        $qb->expects($this->exactly(count($orWhere)))
            ->method('orWhere')
            ->withConsecutive(...array_map(
                    function ($item) {
                        return [$item];
                    },
                    $orWhere
                )
            );

        $qb->method('setParameter')->withConsecutive(...$parameters);

        if ($limit) $qb->expects($this->once())->method('setMaxResults')->with($limit);
        if ($offset) $qb->expects($this->once())->method('setFirstResult')->with($offset);

        $expectedQuery = $this->createStub(Query::class);
        $qb->method('getQuery')->willReturn($expectedQuery);

        $builder = new QueryBuilder($em);

        $this->assertEquals(
            $expectedQuery,
            $builder->getQuery($entityClassName, $filter, $select, $offset, $limit)
        );
    }

    public function getQueryDataProvider()
    {
        return [
            [
                'ClassName',
                [
                    'first_name' => [
                        '_eq' => 'Bob'
                    ]
                ],
                'i',
                [],
                [
                    'i.firstName = :firstName'
                ],
                [],
                [
                    ['firstName', 'Bob']
                ],
                0,
                100
            ],
            [
                'ClassName',
                [
                    'first_name' => [
                        '_eq' => 'Bob'
                    ],
                    'last_name' => [
                        '_eq' => 'Marley'
                    ]
                ],
                'i',
                [],
                [
                    'i.firstName = :firstName',
                    'i.lastName = :lastName'
                ],
                [],
                [
                    ['firstName', 'Bob'],
                    ['lastName', 'Marley']
                ],
                0,
                100
            ],
            [
                'ClassName',
                [
                    '_or' => [
                        'first_name' => [
                            '_eq' => 'Bob'
                        ],
                        'last_name' => [
                            '_eq' => 'Marley'
                        ]
                    ]
                ],
                'i',
                [],
                [],
                [
                    'i.firstName = :firstName',
                    'i.lastName = :lastName'
                ],
                [
                    ['firstName', 'Bob'],
                    ['lastName', 'Marley']
                ],
                0,
                100
            ]
        ];
    }
}
